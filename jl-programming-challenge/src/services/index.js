const apiBaseURL = 'https://api.joblocal.de/v4'

export async function getSearchJobs (
  query, pageNumber
) {
  const queryParam = query !== '' ? `?search.query="${query}"&page.number=${pageNumber}` : `?page.number=${pageNumber}`
  try {
    const response = await fetch(
      `${apiBaseURL}/search-jobs/${queryParam}`
    )
    return response.json()
  } catch (error) {
    console.error(
      `Error making ${apiBaseURL}/search-jobs/${queryParam}" GET request`,
      error
    )
    throw error
  }
}
